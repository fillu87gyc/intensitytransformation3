/* histogram.c */
//#include "pch.h" visual studio で使うときのみ有効化する
#include<stdio.h>
#include <math.h>
#include"pgmlib.h"
long int hist[256]; /* 1d array for histogram*/
double gamma[256];
void make_histogram(int n)
/* generate the histogram for No.n image */
{
	int i, x, y;

	for (i = 0; i < 256; i++) hist[i] = 0;  // initialize the histogram
	for (y = 0; y < height[n]; y++)         // scan the image and count the number of intensity
		for (x = 0; x < width[n]; x++)
			hist[image[n][x][y]]++;
}

void make_histogram_image(int h, int n, int source)
/* generate a histogram image (256 pixel width, h pixel height)*/
/* Save the histogram image in No.n image*/
{
	make_histogram(source);
	int i, x, y;
	long int max; /* max count*/
	int takasa;   /* the height of histogram for each intensity*/

	/* set the size of histogram image */
	width[n] = 256;
	height[n] = h;
	max = 0;
	for (i = 0; i < 256; i++)
		if (hist[i] > max) max = hist[i];
	/* normalize the count between 0 and h */
	for (x = 0; x < width[n]; x++) {
		takasa = (int)(h / (double)max * hist[x]);
		if (takasa > h) takasa = h;
		for (y = 0; y < h; y++)
			if (y < takasa) image[n][x][h - 1 - y] = 0;
			else image[n][x][h - 1 - y] = 255;
	}
}
void reverse_image(int n)
/* reverse the intensity of image No.n */
{
	int x, y;

	for (y = 0; y < height[n]; y++) {
		for (x = 0; x < width[n]; x++) {
			image[n][x][y] = 255 - image[n][x][y];
		}
	}
}
void gamma_trans(double param) {
	for (int i = 0; i < 256; i++)
	{
		gamma[i] = (pow((i / 255.0), 1.0 / param));
	}
}
void make_gamma_curve(int h, int n, double param) {
	int x, y;
	width[n] = height[n] = h;
	int trans_table[256];
	gamma_trans(param);
	for (int i = 0; i < 256; i++)
	{
		trans_table[i] = 255 * gamma[i];
	}
	//h(`)
	for (y = 0; y < height[n]; y++) {
		for (x = 0; x < width[n]; x++) {
			image[n][x][y] = 255;
		}
	}
	//`
	for (int i = 0; i < width[n]; i++)
	{
		image[n][i][h-1-trans_table[i]] = 0;
		printf("%d\n",(int)(trans_table[i]));
	}
}
void make_gamma_filtered_image(int h, int n, double param) {
	int x, y;
	gamma_trans(param);
	for (y = 0; y < height[n]; y++) {
		for (x = 0; x < width[n]; x++) {
			image[n][x][y] = 255*gamma[image[n][x][y]];
		}
	}
}

int main(void)
{
	/*
	No 0 ORG
	No 1 histogram image
	No 2 gamma curves image
	No 3 gamma filtered image
	No 4 gamma filtered image hist
	*/
	double param;
	printf("set  = ");
	scanf("%lf", &param);
	load_image(0, "house.pgm");   /* load image into No.0 */
	for (int i = 1; i < 5; i++)
		copy_image(0, i);
	make_histogram_image(256, 1, 0); /* save the histogram to No.1 image*/
	make_gamma_curve(256, 2,param);
	make_gamma_filtered_image(256, 3,param);
	make_histogram_image(256, 4, 3);
	for (int i = 1; i < 5; i++)
	{
		char name[256];
		name[0] = '0' + i;
		name[1] = '\0';
		printf("%d : ", i );
		save_image(i, strcat(name, ".pgm"));
	}
	return 0;
}
